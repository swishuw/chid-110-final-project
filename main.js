/**
* Sam Wishkoski
* University of Washington
* Chid 110 Final Project
* August 25, 2014
**/

// Global Variables
var paper
var plr
var box
var fg
var grey = false
var axe = false
var spark = false
var sea = false
var onoutput = false

// Main Program
window.onload = function() {
	// Disable Scrolling
	window.onscroll = function () { window.scrollTo(0, 0); };

	// Create Raphael canvas
	paper = new Raphael(document.getElementById('canvas_container'), '100%', '100%');
	width = paper.canvas.offsetWidth;
    height = paper.canvas.offsetHeight;
    var bg = paper.rect(0,0,width,height); // black background
    bg.attr({fill: "black"});
    bg.attr({stroke: "black"});

    // Intro text
    var sttxt = paper.text(width/2, height/2 - height/12, "T Y P E T O D I S C O V E R");
    sttxt.attr({"font-size": 30});
    sttxt.attr({fill: "white"});
    var subtxt = paper.text(width/2, height/2, "d e l e t e / b a c k s  p a c e   t o  t y p e  a g a i n");
    subtxt.attr({"font-size": 14});
    subtxt.attr({fill: "grey"});

    // Global size
    gsize = 3*Math.min(width, height)/4; // global size determined by browser window dimensions
    
    // Establish initial boundary box
    box = paper.rect(width/2 - gsize/10 + gsize/25,height/2 - gsize/10 + gsize/25, gsize/5 - gsize/12 , gsize/5 - gsize/12, 0);
    box.attr({"opacity": 0});


    // Press any key to begin
    window.addEventListener('keydown', function(event) { 
     	// Animate text and begin game
        sttxt.animate({"opacity": 0}, 400);
        subtxt.animate({"opacity": 0}, 400);
     	window.setTimeout(function(){boardinit(paper, gsize)}, 400);
        this.removeEventListener('keydown',arguments.callee,false); // remove any key event listener
        
        // Interactive text box
        var txt = paper.text(width/2, height/2 + height/2.3, "");
        txt.attr({"font-size": 16});
        txt.attr({fill: "white"});

        // Message reply text
        outputtxt = paper.text(width/2, height/2 + height/2.3, "");
        outputtxt.attr({"font-size": 16});
        outputtxt.attr({"fill": "grey"});
        outputtxt.attr({"opacity": 0});

        // Keypress listeners
        var txtlistener = new window.keypress.Listener();
        var dellistener = new window.keypress.Listener();

        // Define input responses and animations
        deftxtinput(txtlistener, dellistener, txt, outputtxt);
	});
    
}

// Initialize the geometry of the board and the player piece
function boardinit(paper, gsize) {
    wallinit(paper, gsize/5, "grey");
    wallinit(paper, gsize/1.5/1.4, "green");
    wallinit(paper, gsize/1.35, "red");
    wallinit(paper, gsize, "blue");
    plrinit(paper, gsize);
}

// Initialize the player piece
function plrinit(paper, gsize) {
	// Create player
	plrsize = gsize/30
	plr = paper.circle(width/2 ,height/2,plrsize, plrsize, 0)
	plr.attr({fill: "white"});
	plr.attr({stroke: "white"});
	plr.attr({"opacity": 0});
    plr.animate({"opacity": 1}, 400);
}

// Initialize walls
function wallinit(paper, size, color) {
    // Create wall
    var wall = paper.rect(width/2 - size/2,height/2 - size/2, size, size, 0)
    wall.attr({stroke: color});
    wall.attr({"stroke-width": 5});
    wall.attr({"opacity": 0});
    wall.animate({"opacity": 1}, 400); 
}

// Adapted from Raphael example code, enables click and
// drag movement and animations (level 0)
function enablemv() {
    var start = function () {
                this.ox = this.attr("cx");
                this.oy = this.attr("cy");
                this.animate({opacity: .75}, 100, ">");
                document.body.style.cursor = 'none';
    },
    move = function (dx, dy) {
        // Adapted to limit movement given the boundary box
        // global variable
        x = plr.getBBox().x;
        x2 = plr.getBBox().x2;
        y = plr.getBBox().y;
        y2 = plr.getBBox().y2;
        bbox = box.getBBox();

        xy = Raphael.isPointInsideBBox(bbox, x,y);
        x2y = Raphael.isPointInsideBBox(bbox, x2,y);
        xy2 = Raphael.isPointInsideBBox(bbox, x,y2);
        x2y2 = Raphael.isPointInsideBBox(bbox, x2,y2);

        if ((xy && x2y && xy2 && x2y2) || sea) {
            this.attr({cx: this.ox + dx, cy: this.oy + dy});
            if (!xy && !x2y && !xy2 && !x2y2) {
                fg = paper.rect(0,0,width,height); // black foreground
                fg.attr({fill: "black"});
                fg.attr({stroke: "black"});
                fg.attr({"opacity": 0});
                fg.animate({"opacity": 1}, 1000);
            }
        } else {
            nowX = Math.min(bbox.x2, this.ox + dx);
            nowY = Math.min(bbox.y2, this.oy + dy);
            nowX = Math.max(bbox.x, nowX);
            nowY = Math.max(bbox.y, nowY);            
            this.attr({cx: nowX, cy: nowY });
        }
    },
    up = function () {
        this.animate({opacity: 1}, 100, ">");
        document.body.style.cursor = 'default';
    };
    paper.set(plr).drag(move, start, up);
}


// Function for beating level 1, enables new dimensions and colors
function enablegrey() {
    plr.animate({"fill": "grey"}, 300);
    plr.animate({"stroke": "grey"}, 300);
    box = paper.rect(width/2 - gsize/1.5/1.4/2 + gsize/25 ,height/2 - gsize/1.5/1.4/2 + gsize/25, gsize/1.5/1.4 - gsize/12, gsize/1.5/1.4 - gsize/12, 0);
    box.attr({"opacity": 0});
    grey = true;
}

// Function for beating level 2, enables new dimensions and colors
function enableaxe() {
    if (grey) {
        box = paper.rect(width/2 - gsize/1.35/2 + gsize/25,height/2 - gsize/1.35/2 + gsize/25, gsize/1.35- gsize/12, gsize/1.35- gsize/12, 0);
        plr.animate({"fill": "green"}, 300);
        plr.animate({"stroke": "green"}, 300);
        box.attr({"opacity": 0});
        axe = true;
    }
}

// Function for beating level 3, enables new dimensions and colors
function enablespark() {
    if (axe) {
        plr.animate({"fill": "red"}, 300);
        plr.animate({"stroke": "red"}, 300);
        box = paper.rect(width/2 - gsize/2+ gsize/25, height/2 - gsize/2+ gsize/25, gsize- gsize/12, gsize- gsize/12, 0); 
        box.attr({"opacity": 0});
        spark = true;
    }   
}

// Function for beating level 4, enables new dimensions and colors
function enablesea() { 
    if (spark) {
        plr.animate({"fill": "blue"}, 300);
        plr.animate({"stroke": "blue"}, 300);
        sea = true;
    }
}

// Define text input responses including animations and specific
// responses
function deftxtinput(txtlistener, dellistener, txt, outputtxt) {
    // Define delete reponse
    deftxtdel(txtlistener, dellistener, txt, outputtxt);
    
    // Define letter response
    alph = "abcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < alph.length; i++) {
        deftxtltr(txtlistener, txt, alph.charAt(i), outputtxt);
    }

    // Intended as level 0 queries
    deftxtkeyword(txtlistener, txt, outputtxt, "h e l l o", "hello", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "r e s e t", "", function(){location.reload(true);});
    deftxtkeyword(txtlistener, txt, outputtxt, "d i s c o v e r", "type to discover", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "t u t o r i a l", "type help to learn more", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "t y p e", "type help to learn more", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "h e l p", "to play, move the player", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "p l a y", "to play, move the player", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "g a m e", "type to discover", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "c i r c l e", "player", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "s q u a r e", "boundaries", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "w o r d", "words transcend", null);
    
    // Intended as level 1 queries
    deftxtkeyword(txtlistener, txt, outputtxt, "m o v e", "click and drag to move the player, boundaries are colored", enablemv);
    deftxtkeyword(txtlistener, txt, outputtxt, "c o l o r", "boundaries are specific colors", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "c o l o r", "boundaries are specific colors", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "g r a y", "try the alternative spelling", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "g r e y", "grey walls are permeable", enablegrey);

    // Intended as level 2 queries
    deftxtkeyword(txtlistener, txt, outputtxt, "g r e e n", "\"see the wood by the means of the trees\"", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "f o r e s t", "composed of trees", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "t r e e", "wood by these means", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "w o o d", "refined by a tool", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "t o o l", "wood from the trees", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "h a t c h e t", "try a different tool", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "a x e", "green walls are permeable", enableaxe);
    
    // Intended as level 3 queries
    deftxtkeyword(txtlistener, txt, outputtxt, "r e d", "glows as an ember", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "m a t c h", "tool to create", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "f i r e", "grows quickly, but started by the smallest", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "s p a r k", "imagination as the spark of inspiration", enablespark);

    // Intended as level 4 queries
    deftxtkeyword(txtlistener, txt, outputtxt, "w a t e r", "beautiful blue", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "b l u e", "dark rolling waters dividing continents", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "o c e a n", "she sang beyond the genius of the", null);
    deftxtkeyword(txtlistener, txt, outputtxt, "s e a", "order the words, order the sea", enablesea);
}

// Define keyword and response, opening new levels using a function parameter fn
function deftxtkeyword(txtlistener, txt, outputtxt, keyword, output, fn) {
    txtlistener.sequence_combo(keyword, function() {
        txt.animate({"opacity": 0}, 300);
        outputtxt.attr({"text": output});
        window.setTimeout(function(){outputtxt.animate({"opacity": 1}, 200);}, 200);
        if (fn !== null) {
            fn();
        }
        txtlistener.stop_listening();
        onoutput = true;
    });
}

// Define delete function (clears current text buffer)
function deftxtdel(txtlistener, dellistener, txt, outputtxt) {
    dellistener.simple_combo("backspace", function() {
        txt.attr({"opacity": 1});
        outputtxt.attr({"text": ""});
        outputtxt.attr({"opacity": 0});
        txt.attr({text: ""});
        txtlistener.listen();
    });
}

// Define letter keys, adding to the current buffer
function deftxtltr(txtlistener, txt, letter, outputtxt) {
    txtlistener.simple_combo(letter, function() {
        currtxt = txt.attr("text");
        newtxt = currtxt.concat(" " + letter.toUpperCase());
        txt.attr({text: newtxt});
    });
}
